# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from itertools import permutations
from queue import Queue
from functools import reduce

    
def getValue(mode, param, p):
    if mode == 0:
        return p[param]
    else:
        return param

def mult(idx, p, modes, **kwargs):
    i1, i2, i3 = p[idx+1:idx+4]
    p[i3] = getValue(int(modes[0]), i1, p) * getValue(int(modes[1]), i2, p)
    return p, 4, None

def add(idx, p, modes, **kwargs):
    i1, i2, i3 = p[idx+1:idx+4]
    p[i3] = getValue(int(modes[0]), i1, p) + getValue(int(modes[1]), i2, p)
    return p, 4, None

def stor(idx, p, modes, ipt = None, **kwargs):
    i = ipt.get()
    p[p[idx+1]] = i
    return p, 2, None

def out(idx, p, modes, **kwargs):
    i = p[idx+1]
    return p, 2, getValue(int(modes[0]), i, p)

def jit(idx, p, modes, **kwargs):
    i1, i2 = p[idx+1:idx+3]
    if getValue(int(modes[0]), i1, p) != 0:
        retJump = getValue(int(modes[1]), i2, p) - idx
    else:
        retJump = 3
    
    return p, retJump, None

def jif(idx, p, modes, **kwargs):
    i1, i2 = p[idx+1:idx+3]
    if getValue(int(modes[0]), i1, p) == 0:
        retJump = getValue(int(modes[1]), i2, p) - idx
    else:
        retJump = 3
    return p, retJump, None

def lst(idx, p, modes, **kwargs):
    i1, i2, i3 = prog[idx+1:idx+4]
    if getValue(int(modes[0]), i1, p) < getValue(int(modes[1]), i2, p):
        p[i3] = 1
    else:
        p[i3] = 0
    return p, 4, None

def eql(idx, p, modes, **kwargs):
    i1, i2, i3 = p[idx+1:idx+4]
    if getValue(int(modes[0]), i1, p) == getValue(int(modes[1]), i2, p):
        p[i3] = 1
    else:
        p[i3] = 0
    return p, 4, None

PROGS = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '99': 'stop'}


def compute(inputq, prog):
#    print(prog)
    outputs = []
    opcodeIdx = 0
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            prog, increment, o = PROGS[op](opcodeIdx, prog, modes, ipt=inputq)
            if o is not None:
                outputs.append(o)
            opcodeIdx += increment
            
    return outputs[-1]

def compute2(inputq, prog, opcodeIdx, outputs):
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            ret = ((inputq, prog.copy(), opcodeIdx, outputs), True, False)
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            if op == '03' and inputq.qsize() == 0:
                ret = ((inputq, prog.copy(), opcodeIdx, outputs), False, True)
                break
            prog, increment, o = PROGS[op](opcodeIdx, prog, modes, ipt=inputq)
        
            if o is not None:
                outputs.put(o)
            opcodeIdx += increment

    return ret

def getNextIterator(i):
    if i == 4:
        return 0
    else:
        return i+1
    
with open('./inputs/day7.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog) 
#PROG = np.array([3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5])

## Part 1
maxThrust = 0
inputs = Queue()
phases = None

for p in permutations(range(5)):
    output = 0
    for i in p:
        inputs.put(i)
        inputs.put(output)
        output = compute(inputs, PROG.copy())
    if output > maxThrust:
        maxThrust = output
        phases = p

print(maxThrust)

## Part 2
maxThrust = 0

thruster = {'idx': 0,
            'input': None,
            'output': None,
            'progState': None,
            'completed': False,
            'stalled': False}

for p in permutations(range(5, 10)):
    thrusters = [None]*5
    for n, i in enumerate(p):
        t = thruster.copy()
        t['input'] = Queue()
        t['output'] = Queue()
        t['input'].put(i)
        t['progState'] = PROG.copy()
        thrusters[n] = t.copy()
    iterator = 0
    thrusters[0].get('input').put(0)
#    print(thrusters[0].get('input').qsize())
    while not thrusters[-1].get('completed'):
        th = thrusters[iterator]
        if th['stalled'] and th['input'].qsize()==0:
            iterator = getNextIterator(iterator)
            continue
        state, complete, stall = compute2(th['input'], 
                                          th['progState'], 
                                          th['idx'], 
                                          th['output'])
        th['completed'] = complete
        th['stalled'] = stall
#        i, p, idx, o = state
        th['idx'] = state[2]
        th['progState'] = state[1][:]
        itr = getNextIterator(iterator)
        otp = th['output']
        while otp.qsize() > 0:
            o = otp.get()
            if not thrusters[itr].get('completed'):
                thrusters[itr].get('input').put(o)
                thrusters[itr]['stalled'] = False
        iterator = itr
        if reduce(lambda x,y: x and y, [t.get('stalled') for t in thrusters]):
            break
        
    finalOutput = o
    if finalOutput > maxThrust:
        maxThrust = finalOutput
        phases = p

print(maxThrust)