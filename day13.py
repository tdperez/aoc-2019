# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from itertools import permutations
from queue import Queue
from functools import reduce

MEMORY = {}

def getPosition(mode, param, p, offset):
    if mode == 2:
        ret = param + offset
    else:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = param
    
    return p, ret

def write(p, position, value):
    if position >= len(p):
        MEMORY[position] = value
    else:
        p[position] = value
    return p

def getValue(mode, param, p, offset):
    if mode == 0:
        if param >= len(p):
#            p.resize(param + 1, refcheck=False)
            ret = MEMORY.get(param, 0)
        else:
            ret = p[param]
        
    elif mode == 2:
        if param + offset >= len(p):
            ret = MEMORY.get(param+offset, 0)
        else:
            ret = p[param + offset]        
    else:
        if param >= len(p):
            ret = param
        else:
            ret = param
    
    return p, ret

def mult(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    p = write(p, v3, v1*v2)
#    p[v3] = v1 * v2
    return p, 4, None, None

def add(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
#    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])

    p = write(p, v3, v1+v2)
    return p, 4, None, None

def stor(idx, p, modes, ipt = None, **kw):
    i = ipt.get()
#    print(idx, modes[0], i, kw['offset'])
    p, pos = getPosition(int(modes[0]), p[idx+1], p, kw['offset'])
#    print(pos)
#    p[pos] = i
    p = write(p, pos, i)
    return p, 2, None, None

def out(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset']) #p[idx+1]
    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, v, None

def jit(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
#    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v != 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    
    return p, retJump, None, None

def jif(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v == 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    return p, retJump, None, None

def lst(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 < v2:
        p = write(p, v3, 1) #p[v3] = 1
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def eql(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 == v2:
        p = write(p, v3, 1)
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def ost(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset'])

    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, None, v

PROGS = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '09': ost,
         '99': 'stop'}

def compute2(inputq=None, prog=[], opcodeIdx=0, outputs=None, offset=0):
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), True, False)
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            if op == '03' and inputq.qsize() == 0:
                ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), False, True)
                break
            
            try:
                prog, increment, o, a = PROGS[op](opcodeIdx, prog, modes, ipt=inputq, offset=offset)
            except Exception as e:
                print(op, modes, opcodeIdx, len(prog))
                raise e
                
            if o is not None:
                outputs.put(o)
            
            if a is not None:
                offset += a
            opcodeIdx += increment

    return ret

def getNextIterator(i):
    if i == 4:
        return 0
    else:
        return i+1

 
with open('./inputs/day13.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog, dtype=np.int64) 
#PROG = np.array([1102,34915192,34915192,7,4,7,99,0], dtype=np.int64)

inpt = Queue()
otpt = Queue()
board = {}
## Part 1
state, complete, stalled = compute2(inpt, PROG.copy(), 0, otpt, 0)
blockCount = 0
while not otpt.empty():
    x = otpt.get()
    y = otpt.get()
    tile = otpt.get()
    board[(x,y)] = tile
    if tile == 2:
        blockCount += 1
        
print(f'Number of blocks: {blockCount}')

## Part 2
easyMode = True
if easyMode:
    file = './inputs/day13_mod.txt'
else:
    file = './inputs/day13.txt'
    
with open(file, 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog, dtype=np.int64) 

MEMORY = {}
blockCount = 0
#((inputq, prog.copy(), opcodeIdx, outputs, offset), False, True)
prog = PROG.copy()
prog[0] = 2
offset = 0
idx = 0
rnd = 1
ballMotion = {'heading':0,
              'prev': 0}

display = np.empty((35,42), dtype=str)
display[:] = ''
tileMapping = {0: ' ',
               1: '8',
               2: 'X',
               3: '-',
               4: 'o'}

while blockCount > 0 or rnd == 1:

    state, complete, stalled = compute2(inpt, prog, idx, otpt, offset)
    _, prog, idx, _, offset = state
    
    while not otpt.empty():
        x = otpt.get()
        y = otpt.get()
        tile = otpt.get()
        if tile == 2:
            blockCount += 1
        elif tile == 3:
            paddle = (x,y)
        elif tile == 4:
            ball = (x,y)
        elif tile == 0 and board.get((x,y)) == 2:
            blockCount -= 1
        board[(x,y)] = tile
        
        if x >= 0:
            display[y,x]= tileMapping.get(tile)
    if easyMode:
        inpt.put(0)
        rnd += 1
        continue
    else:
        if ball[0] < ballMotion.get('prev', 0):
            ballMotion['heading'] = -1
        elif ball[0] > ballMotion.get('prev',0):
            ballMotion['heading'] = 1
        else:
            ballMotion['heading'] = 0
        ballMotion['prev'] = ball[0]
        
        if ball[0] == paddle[0]:
            if ball[1]+1==paddle[1]:
                joystick = 0
            else:
                joystick = ballMotion['heading']
        else:
            joystick = (ball[0] - paddle[0])/abs(ball[0] - paddle[0])
            
        inpt.put(joystick)
        
        rnd += 1
    #    print('\n'.join([''.join(list(l)) for l in display]))
        if ball[1] == paddle[1]:
            print(blockCount)
            break

print(f'Final score: {board.get((-1,0))}')        