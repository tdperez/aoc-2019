# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np

def getValue(mode, param, prog):
    if mode == 0:
        return prog[param]
    else:
        return param

def mult(idx, prog, modes, **kwargs):
    i1, i2, i3 = prog[idx+1:idx+4]
    prog[i3] = getValue(int(modes[0]), i1, prog) * getValue(int(modes[1]), i2, prog)
    return prog, 4, None

def add(idx, prog, modes, **kwargs):
    i1, i2, i3 = prog[idx+1:idx+4]
    prog[i3] = getValue(int(modes[0]), i1, prog) + getValue(int(modes[1]), i2, prog)
    return prog, 4, None

def stor(idx, prog, modes, ipt = None, **kwargs):
    prog[prog[idx+1]] = ipt
    return prog, 2, None

def out(idx, prog, modes, **kwargs):
    i = prog[idx+1]
    return prog, 2, getValue(int(modes[0]), i, prog)

def jit(idx, prog, modes, **kwargs):
    i1, i2 = prog[idx+1:idx+3]
    if getValue(int(modes[0]), i1, prog) != 0:
        retJump = getValue(int(modes[1]), i2, prog) - idx
    else:
        retJump = 3
    
    return prog, retJump, None

def jif(idx, prog, modes, **kwargs):
    i1, i2 = prog[idx+1:idx+3]
    if getValue(int(modes[0]), i1, prog) == 0:
        retJump = getValue(int(modes[1]), i2, prog) - idx
    else:
        retJump = 3
    return prog, retJump, None

def lst(idx, prog, modes, **kwargs):
    i1, i2, i3 = prog[idx+1:idx+4]
    if getValue(int(modes[0]), i1, prog) < getValue(int(modes[1]), i2, prog):
        prog[i3] = 1
    else:
        prog[i3] = 0
    return prog, 4, None

def eql(idx, prog, modes, **kwargs):
    i1, i2, i3 = prog[idx+1:idx+4]
    if getValue(int(modes[0]), i1, prog) == getValue(int(modes[1]), i2, prog):
        prog[i3] = 1
    else:
        prog[i3] = 0
    return prog, 4, None

with open('./inputs/day5.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

prog = np.array(prog[:]) 
#prog = np.array([])
progs = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '99': 'stop'}

INPUT = 5 #input('Enter program ID: ')
outputs = []
opcodeIdx = 0
while opcodeIdx < len(prog):
    if prog[opcodeIdx] == 99:
        break
    else:
        opcode = f'{prog[opcodeIdx]:05}'
        op = opcode[3:5]
        modes = opcode[2::-1]
        prog, increment, o = progs[op](opcodeIdx, prog, modes, ipt=INPUT)
        if o is not None:
            outputs.append(o)
        opcodeIdx += increment

print(outputs)