# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np


def nearer(pt1, pt2):
    if sum(pt1) < sum(pt2):
        return {pt1: pt2}
    else:
        return {pt2: pt1}
    
def sameSide(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    return (x1 > 0 and x2 > 0) or (x1 < 0 and x2 < 0) or (y1 < 0 and y2 < 0) or (y1 > 0 and y2 > 0)

def getAngle(x, y):
    if x==0:
        if y > 0:
            ang = np.pi
        else:
            ang = 2*np.pi
    elif y==0:
        if x > 0:
            ang = 3*np.pi/2
        else:
            ang = np.pi/2
    else:
        if x > 0:
            if y > 0:
                ang = np.arctan(y/x) + np.pi
            else:
                ang = abs(np.arctan(y/x)) + 3*np.pi/2
        else:
            if y > 0:
                ang = np.arctan(y/x) + np.pi
            else:
                ang = np.pi/2 - np.arctan(y/x) 
    return ang
    
with open('./inputs/day10.txt', 'r') as f:
    data = f.read()

a = []
for y, line in enumerate(data.split('\n')):
    for x, pnt in enumerate(line):
        if pnt == '#':
            a.append(np.array([x,y]))

asteroids = np.array(a)
maxX = max([d[0] for d in a])
maxY = max([d[1] for d in a])

n = len(a)

sights = {}
## Part 1
for centroid in asteroids:
    recentered = list(filter(lambda x: x.any(), asteroids - centroid))
    sights[tuple(centroid)] = {}
    tobeprocessed = set([tuple(p) for p in recentered])
    for ast1 in recentered:
        if (ast1[0] == 0 and ast1[1] == 0) or tuple(ast1) not in tobeprocessed:
            continue
        s1 = [tuple(ast1+centroid)]
        s2 = []
        for ast2 in recentered:
            if tuple(ast1) == tuple(ast2) or (ast1[0] == 0 and ast1[1] == 0):
                continue
            else:
                if ast1[0]*ast2[1] == ast2[0]*ast1[1]:
                    if sameSide(ast1, ast2):
                        s1.append(tuple(ast2+centroid))
                    else:
                        s2.append(tuple(ast2+centroid))
                    tobeprocessed.remove(tuple(ast2))
                    
        sights[tuple(centroid)][tuple(ast1)] = {1: s1.copy(),
                                                2: s2.copy()}
        
maxBodies = 0
bestSite = (0,0)
for cent in sights:
    currentBodies = 0
    for angle in sights.get(cent).keys():
        for bodies in sights.get(cent).get(angle).keys():
            if len(sights.get(cent).get(angle).get(bodies)) > 0:
                currentBodies += 1
    if currentBodies > maxBodies:
        maxBodies = currentBodies
        bestSite = cent
        
print(f'Best site at {bestSite} with {maxBodies} viewable bodies')
    

## Part 2
centroid = (19,11)
site = sights[centroid]

angles = {}
cnt = 0
for line in site.keys():
    l = site.get(line)
    for angle in l.keys():
        pnts = l.get(angle)
        if len(pnts) == 0:
            continue
        
        rep = tuple(np.array(pnts[0]) - np.array(centroid))
        p1, p2 = rep
        if p1 < 0 and p2 < 0:
            rad = getAngle(*rep)
            angles[rad] = pnts
        else:
            cnt += 1
#        print(line, angle, rep, rad)
#        angles[rad] = pnts
        
radians = list(angles.keys())
radians.sort(reverse=True)
print(angles[radians[199-cnt]])