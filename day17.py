# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from itertools import permutations
from queue import Queue
from functools import reduce

MEMORY = {}

def getPosition(mode, param, p, offset):
    if mode == 2:
        ret = param + offset
    else:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = param
    
    return p, ret

def write(p, position, value):
    if position >= len(p):
        MEMORY[position] = value
    else:
        p[position] = value
    return p

def getValue(mode, param, p, offset):
    if mode == 0:
        if param >= len(p):
#            p.resize(param + 1, refcheck=False)
            ret = MEMORY.get(param, 0)
        else:
            ret = p[param]
        
    elif mode == 2:
        if param + offset >= len(p):
            ret = MEMORY.get(param+offset, 0)
        else:
            ret = p[param + offset]        
    else:
        if param >= len(p):
            ret = param
        else:
            ret = param
    
    return p, ret

def mult(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    p = write(p, v3, v1*v2)
#    p[v3] = v1 * v2
    return p, 4, None, None

def add(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
#    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])

    p = write(p, v3, v1+v2)
    return p, 4, None, None

def stor(idx, p, modes, ipt = None, **kw):
    i = ipt.get()
#    print(idx, modes[0], i, kw['offset'])
    p, pos = getPosition(int(modes[0]), p[idx+1], p, kw['offset'])
#    print(pos)
#    p[pos] = i
    p = write(p, pos, i)
    return p, 2, None, None

def out(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset']) #p[idx+1]
    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, v, None

def jit(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
#    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v != 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    
    return p, retJump, None, None

def jif(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v == 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    return p, retJump, None, None

def lst(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 < v2:
        p = write(p, v3, 1) #p[v3] = 1
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def eql(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 == v2:
        p = write(p, v3, 1)
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def ost(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset'])

    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, None, v

PROGS = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '09': ost,
         '99': 'stop'}

def compute2(inputq=None, prog=[], opcodeIdx=0, outputs=None, offset=0):
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), True, False)
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            if op == '03' and inputq.qsize() == 0:
                ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), False, True)
                break
            
            try:
                prog, increment, o, a = PROGS[op](opcodeIdx, prog, modes, ipt=inputq, offset=offset)
            except Exception as e:
                print(op, modes, opcodeIdx, len(prog))
                raise e
                
            if o is not None:
                outputs.put(o)
            
            if a is not None:
                offset += a
            opcodeIdx += increment

    return ret

def getNextIterator(i):
    if i == 4:
        return 0
    else:
        return i+1

def drawRoom(data):
        pnts = list(data.keys())
        xs = list([p[0] for p in pnts])
        ys = list([p[1] for p in pnts])
        minX = min(xs)
        minY = min(ys)
        maxX = max(xs)
        maxY = max(ys)
        xSize = maxX - minX
        ySize = maxY - minY
        room = np.empty((ySize+1, xSize+1), dtype=str)
        room[:] = ' '
        for p in pnts:
            roomCoor = (p[1]-minY,p[0]-minX)
            room[roomCoor] = data[p]
        return '\n'.join([''.join(list(l)) for l in room])
 
with open('./inputs/day17.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog, dtype=np.int64) 
#PROG = np.array([1102,34915192,34915192,7,4,7,99,0], dtype=np.int64)

inpt = Queue()
otpt = Queue()
board = {}
## Part 1
state, complete, stalled = compute2(inpt, PROG.copy(), 0, otpt, 0)
display = {}
y = 0
x = 0
scaffolds = set()
while not otpt.empty():
    value = otpt.get()
    if chr(value) == '\n':
        y += 1
        x = 0
    else:
        display[(x,y)] = chr(value)
        if chr(value) == '#':
            scaffolds.add((x,y))
        x += 1
    
alignment = 0
for pnt in scaffolds:
    x,y = pnt
    north = (x, y-1)
    south = (x,y+1)
    east = (x+1, y)
    west = (x-1, y)
    if north in scaffolds and south in scaffolds and east in scaffolds and west in scaffolds:
        alignment += x*y
    
print(alignment)

## Part 2
######### Get path #########
for pnt in display.keys():
    if display[pnt] not in ('#','.'):
        start = pnt

heading = 'n'
steps = {'n': [0,-1],
         's': [0,1],
         'e': [1,0],
         'w': [-1,0]}

turns = {'n': ['e','w'],
         's': ['e','w'],
         'e': ['n','s'],
         'w': ['n','s']}

leftRight = {('n','e'): 'r',
             ('n','w'): 'l',
             ('s','e'): 'l',
             ('s','w'): 'r',
             ('e','n'): 'l',
             ('e','s'): 'r',
             ('w','s'): 'l',
             ('w','n'): 'r'}

def canStep(direction, location):
    s = steps.get(direction)
    if s is not None:
        lat, long = s
        x, y = location
        if (x+lat, y+long) in scaffolds:
            return True
        else:
            return False
    else:
        return False
    
def turn(d, l):
    for pos in turns.get(d):
        if canStep(pos, l):
            return leftRight.get((d,pos)), pos
    return '', ''

def step(d, l):
    lat, long = steps.get(d)
    x, y = l
    return (x+lat, y+long)

path = []
numSteps = 0
while heading != '' or canStep(heading, start):
    if canStep(heading, start):
        numSteps += 1
        start = step(heading, start)
    else:
        if numSteps != 0:
            path.append(numSteps)
        t, h = turn(heading, start)

        if t:
            path.append(t)
        heading = h
        numSteps = 0


######### Generic Find Routines #########
def toString(l):
    return ''.join(list(map(str, l)))

def rfToPath(l):
    rec = []
    for inst in l:
        rec = rec + fxns.get(inst, [])
    return rec

for a in range(5, 12):
    fxns = {'A': [],
            'B': [],
            'C': []}
    rf = []
    A = path[0:a]
    fxns['A'] = A.copy()
    i = 0
    while True:
        if i+a <= len(path) and toString(A) == toString(path[i:i+a]):
            rf.append('A')
            i += a
        else:
            q = i
            for bstep in range(2, 12):
                i = q
                B = path[i:i+bstep]
                fxns['B'] = B.copy()
                while True:
                    if i+bstep <= len(path) and toString(path[i:i+bstep]) == toString(B):
                        rf.append('B')
                        i += bstep
                    elif toString(path[i:i+a]) == toString(A):
                        rf.append('A')
                        i+= a
                    else:
                        r = i
                        for cstep in range(2, 12):
                            i = r
                            C = path[i:i+cstep]
#                            print(C)
                            fxns['C'] = C.copy()
                            while True:
                                if i+cstep <= len(path) and toString(C) == toString(path[i:i+cstep]):
                                    rf.append('C')
                                    i += cstep
                                elif i+bstep <= len(path) and toString(path[i:i+bstep]) == toString(B):
                                    rf.append('B')
                                    i += bstep
                                elif i+a <= len(path) and toString(path[i:i+a]) == toString(A):
                                    rf.append('A')
                                    i += a
                                else:
                                    break
                            if toString(rfToPath(rf)) == toString(path):
                                break
                            else:
                                if 'C' in rf:
                                    rf = rf[:rf.index('C')]
                        break
                        
                if toString(rfToPath(rf)) == toString(path):
                    break
                else:
                    if 'B' in rf:
                        rf = rf[:rf.index('B')]
            break
                
    if toString(rfToPath(rf)) == toString(path):
        break
    
##### Send routines ##########
newProg = PROG.copy()
newProg[0] = 2
realTime = ['n']
inpt2 = Queue()
otpt2 = Queue()
for instSet in [rf, fxns['A'], fxns['B'], fxns['C']]:
    instStr = ','.join(list(map(str, instSet)))
    instStr = instStr.upper()
    print(instStr)
    for char in instStr:
        inpt2.put(ord(char))
    inpt2.put(ord('\n'))

inpt2.put(ord('n'))
inpt2.put(ord('\n'))

state, complete, stalled = compute2(inpt2, newProg, 0, otpt2, 0)

output = []
while not otpt2.empty():
    output.append(otpt2.get())

print(output[-1])