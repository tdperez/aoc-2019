# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np

def mult(x, y):
    return x*y

def add(x, y):
    return x+y

with open('./inputs/day2.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

stable = prog[:] 

progs = {1: add,
         2: mult,
         99: 'stop'}

found = False
for n in range(99):
    for v in range(99):
        prog = stable[:]
        prog[1] = n
        prog[2] = v


        opcodeIdx = 0
        while opcodeIdx < len(prog):
            if prog[opcodeIdx] == 99:
                break
            else:
                i1 = prog[opcodeIdx+1]
                i2 = prog[opcodeIdx+2]
                i3 = prog[opcodeIdx+3]
                prog[i3] = progs[prog[opcodeIdx]](prog[i1], prog[i2])
            opcodeIdx += 4

        if prog[0] == 19690720:
            print(f'Noun: {n}\nVerb: {v}\n{100*n+v}')
            found=True
        if found:
            break
    if found:
        break
#print(prog)