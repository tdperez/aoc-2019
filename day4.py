# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""
import re
import numpy as np

def hasDouble(n):
    n = str(n)
    double = False
    for i in range(len(n)-1):
        if n[i] == n[i+1]:
            double = True
            break
    return double

def increasing(n):
    n = str(n)
    prev = '0'
    increasing = True
    
    for d in range(len(n)):
        if n[d] >= prev:
            prev = n[d]
        else:
            increasing = False
            break
    return increasing

def singleDoubleSet(n, pattern):
    matches = pattern.findall(str(n))
    return min(map(len, matches))==2

mn = 138241
mx = 674034

hd = np.vectorize(hasDouble)
inc = np.vectorize(increasing)

a = np.arange(mn, mx)
i = a[inc(a)]

possibles = i[hd(i)]

print(len(possibles))

second = []
pattern = re.compile('(1{2,}|2{2,}|3{2,}|4{2,}|5{2,}|6{2,}|7{2,}|8{2,}|9{2,})')
for possible in possibles:
    if singleDoubleSet(possible, pattern):
        second.append(possible)

print(len(second))