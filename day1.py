# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np




with open('./inputs/day1.txt', 'r') as f:
    l = f.readlines()
    
modules = np.array([int(m.replace('\n','')) for m in l])
    
print(((modules//3)-2).sum())

fuel = modules//3 - 2 
total = 0
while fuel.sum() > 0:
    total += fuel.sum()
    fuel = fuel//3 - 2
    fuel[fuel<0] = 0
    
print(total)