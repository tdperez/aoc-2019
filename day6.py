# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from queue import PriorityQueue

def getOrbitCounts(key, o):
    i = 0
    d = 0
    c = key
    while c != 'COM':
        if c == key:
            c = o.get(key)
            d += 1
        else:
            i += 1
            c = o.get(c)
            
    return i, d

with open('./inputs/day6.txt', 'r') as f:
    obts = list(f.read().split('\n'))

#obts = ['COM)B', 'B)C','C)D','D)E','E)F','B)G','G)H','D)I','E)J','J)K','K)L','K)YOU','I)SAN']

obts = list(map(lambda x: tuple(x.split(')')), obts))

orbits = {v: k for k, v in obts}
orbiteds = {}
for k, v in obts:
    orbiteds.setdefault(k,[]).append(v)

## Part 1
indirectCounts = 0
directCounts = 0
for k in orbits.keys():
    i, d = getOrbitCounts(k, orbits)
    indirectCounts += i
    directCounts += d
print(f'Total orbits: {indirectCounts+directCounts}')

q = PriorityQueue()

start = orbits.get('YOU')
dest = orbits.get('SAN')

path = [start]
sanDist = sum(getOrbitCounts(dest, orbits))
counter = 0
while path[-1] != dest:
    counter += 1
    start = path[-1]
    setPath = set(path)
    inNext = orbits.get(start)
    outNexts = orbiteds.get(start, [])
    if inNext is not None and inNext not in setPath:
        inDist = sum(getOrbitCounts(inNext, orbits)) + sanDist
        q.put((inDist+len(path), path[:]+[inNext]))
    for outNext in outNexts:
        if outNext is not None and outNext not in setPath:
            outDist = sum(getOrbitCounts(outNext, orbits)) + sanDist
            q.put((outDist+len(path), path[:]+[outNext]))
    if q.qsize() > 0:
        _, path = q.get()
    else:
        break
    
print(f'Total jumps: {len(path) - 1}\nFound in {counter} steps.')