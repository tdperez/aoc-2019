# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
'''
    <x=-1, y=0, z=2>
    <x=2, y=-10, z=-7>
    <x=4, y=-8, z=8>
    <x=3, y=5, z=-1>
'''

def multiply(l):
    r = 1
    for i in l:
        r = r*i
    return r

def cycleMoon(positions, velocities):
    for p, pos in enumerate(positions):
        vel = np.zeros(positions.shape, dtype=np.int32)
        vel[positions>pos] = 1
        vel[positions<pos] = -1
        velocities[p] = velocities[p] + np.sum(vel, axis = 0)
    positions = positions + velocities
    
    return positions, velocities

def stateMoons(p, v):
    states = []
    for i in range(3):
#        states.append(tuple(list(p[i] + list(v[i]))))
        states.append(tuple(list(p[:, i]) + list(v[:, i])))
    return states

def factor(n):
    factors = []
    primes = np.zeros(int(n)+1)
    for i in range(len(primes)):
        if i==0 or i == 1:
            continue
        if primes[i] == 0:
            q = 2
            while i*q < len(primes):
                primes[i*q] = 1
                q+=1
            if n%i == 0:
                factors.append(i)
    return factors

with open('./inputs/day12.txt', 'r') as f:
    data = f.read().replace('<','').replace('>','').split('\n')

positions = np.zeros((4,3), dtype=np.int32)

for i, pos in enumerate(data):
    coords = pos.split(',')
    location = np.array([int(coord.split('=')[-1]) for coord in coords], dtype=np.int32)
    positions[i] = location

#positions = np.array([[-1,0,2],[2,-10,-7],[4,-8,8],[3,5,-1]])
#positions = np.array([[-8,-10,0],[5,5,10],[2,-7,3],[9,-8,-3]])
starting = positions.copy()

## Part 1
velocities = np.zeros(positions.shape, dtype=np.int32)

for i in range(1000):
    positions, velocities = cycleMoon(positions, velocities)

potential = np.sum(abs(positions), axis=1)
kinetic = np.sum(abs(velocities), axis=1)
print(f'Total Energy: {(potential*kinetic).sum()}')

## Part 2
positions = starting.copy()
velocities = np.zeros(positions.shape, dtype=np.int32)

state = stateMoons(positions, velocities)
moonstates = [set() for i in range(3)]
xPeriodFound, yPeriodFound, zPeriodFound = False, False, False
while not (xPeriodFound and yPeriodFound and zPeriodFound):
    positions, velocities = cycleMoon(positions, velocities)
    state = stateMoons(positions, velocities)
    for m, s in enumerate(state):
        if s in moonstates[m]:
            if m == 0:
                xPeriodFound = True
            elif m==1:
                yPeriodFound = True
            else:
                zPeriodFound = True
        else:
            moonstates[m].add(s)

 
print(list(map(lambda x: [len(x), factor(len(x))], moonstates)))
stateDict = {len(x): factor(len(x)) for x in moonstates}
factors = []
for k, v in stateDict.items():
    facts = v
    f1 = []
    if facts[0] in factors:
        t = k
        while multiply(factors)%multiply(f1) == 0:
            f1.append(facts[0])
            t = t/facts[0]
            facts = factor(t)
        t = t*f1[-1]
        facts = factor(t)
    else:
        t = k
    while multiply(factors)%k != 0:
        factors.append(facts[0])
        t = t/facts[0]
        facts = factor(t)
        
print(multiply(factors))
        
        