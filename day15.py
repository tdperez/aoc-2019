# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from itertools import permutations
from queue import Queue
from functools import reduce
import random as rand

MEMORY = {}

def getPosition(mode, param, p, offset):
    if mode == 2:
        ret = param + offset
    else:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = param
    
    return p, ret

def write(p, position, value):
    if position >= len(p):
        MEMORY[position] = value
    else:
        p[position] = value
    return p

def getValue(mode, param, p, offset):
    if mode == 0:
        if param >= len(p):
#            p.resize(param + 1, refcheck=False)
            ret = MEMORY.get(param, 0)
        else:
            ret = p[param]
        
    elif mode == 2:
        if param + offset >= len(p):
            ret = MEMORY.get(param+offset, 0)
        else:
            ret = p[param + offset]        
    else:
        if param >= len(p):
            ret = param
        else:
            ret = param
    
    return p, ret

def mult(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    p = write(p, v3, v1*v2)
#    p[v3] = v1 * v2
    return p, 4, None, None

def add(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
#    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])

    p = write(p, v3, v1+v2)
    return p, 4, None, None

def stor(idx, p, modes, ipt = None, **kw):
    i = ipt.get()
#    print(idx, modes[0], i, kw['offset'])
    p, pos = getPosition(int(modes[0]), p[idx+1], p, kw['offset'])
#    print(pos)
#    p[pos] = i
    p = write(p, pos, i)
    return p, 2, None, None

def out(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset']) #p[idx+1]
    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, v, None

def jit(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
#    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v != 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    
    return p, retJump, None, None

def jif(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v == 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    return p, retJump, None, None

def lst(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 < v2:
        p = write(p, v3, 1) #p[v3] = 1
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def eql(idx, p, modes, **kw):
    p, i1 = getValue(0, idx+1, p, kw['offset'])
    p, i2 = getValue(0, idx+2, p, kw['offset'])
    p, i3 = getValue(0, idx+3, p, kw['offset'])
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 == v2:
        p = write(p, v3, 1)
    else:
        p = write(p, v3, 0) #p[v3] = 0
    return p, 4, None, None

def ost(idx, p, modes, **kw):
    p, i = getValue(0, idx+1, p, kw['offset'])

    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, None, v

PROGS = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '09': ost,
         '99': 'stop'}

def compute2(inputq=None, prog=[], opcodeIdx=0, outputs=None, offset=0):
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), True, False)
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            if op == '03' and inputq.qsize() == 0:
                ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), False, True)
                break
            
            try:
                prog, increment, o, a = PROGS[op](opcodeIdx, prog, modes, ipt=inputq, offset=offset)
            except Exception as e:
                print(op, modes, opcodeIdx, len(prog))
                raise e
                
            if o is not None:
                outputs.put(o)
            
            if a is not None:
                offset += a
            opcodeIdx += increment

    return ret

def getNextIterator(i):
    if i == 4:
        return 0
    else:
        return i+1


class Droid(object):
    def __init__(self, iQ, oQ, program):
        self.iq = iQ
        self.oq = oQ
        self.p = program
        self.opIdx = 0
        self.offset = 0
        self.position = (0,0)
        self.room = {(0,0): 1}
        self.pathToO2 = []
        self.foundO2 = False
    
    def getRequestedPosition(self, direction):
        pntMap = {'n': np.array([0,-1], dtype=np.int32),
                  's': np.array([0,1], dtype=np.int32),
                  'e': np.array([1,0], dtype=np.int32),
                  'w': np.array([-1,0], dtype=np.int32)}
        return tuple(np.array(self.position, dtype=np.int32) + pntMap.get(direction))
    
    def walk(self, direction):
        dirMap = {'n':1,'s':2,'w':3,'e':4}
        target = self.getRequestedPosition(direction)
        walked = 0
        if self.room.get(target, 1) in [1,2]:
            self.iq.put(dirMap.get(direction))
            state, complete, stalled = compute2(self.iq, self.p, self.opIdx, self.oq, self.offset)
            _, self.p, self.opIdx, _, self.offset = state
            walked = self.updatePosition(target, self.oq.get(), direction)
        return walked
        
    def getPossibleMoves(self):
        moves = []
        for d in 'nsew':
            p = self.getRequestedPosition(d)
            if self.room.get(p, 1) == 1:
                moves.append(d)
        
        return moves
    
    def updatePosition(self, targetPosition, stepCode, step):
        if stepCode in [1,2]:
            self.position = targetPosition
            np = 1
            if not self.foundO2 and targetPosition in self.room:
                self.pathToO2 = self.pathToO2[:-1]
            elif not self.foundO2:
                self.pathToO2.append(step)
            if stepCode == 2:
                self.foundO2 = True
        else:
            np = 0
        self.room[targetPosition] = stepCode
        return np
            
    def drawRoom(self):
        pnts = list(self.room.keys())
        xs = list([p[0] for p in pnts])
        ys = list([p[1] for p in pnts])
        minX = min(xs)
        minY = min(ys)
        maxX = max(xs)
        maxY = max(ys)
        xSize = maxX - minX
        ySize = maxY - minY
        room = np.empty((ySize+1, xSize+1), dtype=str)
        room[:] = ' '
        for p in pnts:
            roomCoor = (p[1]-minY,p[0]-minX)
            if p == self.position:
                room[roomCoor] = 'D'
            elif p == (0,0):
                room[roomCoor] = 'x'
            elif self.room[p] == 1:
                room[roomCoor] = '.'
            elif self.room[p] == 0:
                room[roomCoor] = '#'
            elif self.room[p] == 2:
                room[roomCoor] = 'O'
            else:
                room[roomCoor] = 'X'
        return '\n'.join([''.join(list(l)) for l in room])
    
    def explore(self, steps=10):
        nsteps = 0
        while nsteps < steps:
            moves = self.getPossibleMoves()
            randDir = rand.choice(moves)
            self.walk(randDir)
            nsteps += 1
            
    def exploratoryWalk(self, steps=10):
        heading = 's'
        check = {'s': 'e',
                 'w': 's',
                 'n': 'w',
                 'e': 'n'}
        turn = {'s': 'w',
                'w': 'n',
                'n': 'e',
                'e': 's'}
        nsteps = 0
        while nsteps < steps:
            rp = self.getRequestedPosition(heading)

            stepped = self.walk(check.get(heading))
            if stepped == 1:
                heading = check.get(heading)
                
            elif self.room.get(rp, 1) in [0,2]:
                heading = turn[heading]
                self.walk(heading)
                
            else:
                self.walk(heading)
                
#            print(self.drawRoom())
            nsteps += 1


with open('./inputs/day15.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog, dtype=np.int64) 
#PROG = np.array([1102,34915192,34915192,7,4,7,99,0], dtype=np.int64)

inpt = Queue()
otpt = Queue()
room = {}
## Part 1
droid = Droid(inpt, otpt, PROG.copy())
droid.exploratoryWalk(1000)

print(droid.drawRoom())
print(len(droid.pathToO2))

## Part 2
droid.exploratoryWalk(2000)
print(droid.drawRoom())
room = droid.room.copy()
halls = set()
for coord in room.keys():
    if room[coord] == 2:
        o2 = [coord]
    if room[coord] == 1:
        halls.add(coord)

spreads = [np.array([0,-1], dtype=np.int32),
           np.array([0,1], dtype=np.int32),
           np.array([1,0], dtype=np.int32),
           np.array([-1,0], dtype=np.int32)]

timer = 0   
while len(halls) > 0:
    newAir = []
    for freshAir in o2:
        fa = np.array(freshAir, dtype=np.int32)
        for spread in spreads:
            s = tuple(fa+spread)
            if s in halls:
                newAir.append(s)
                halls.remove(s)
    o2 = newAir[:]
    timer+=1

print(timer)