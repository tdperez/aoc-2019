# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import time

def returnSteps(constant, variant, dist, direction):
    const = [constant]*dist
    if direction in ['U', 'R']:
        steps = range(variant, variant + dist, 1)
    else:
        steps = range(variant, variant - dist, -1)
        
    if direction in ['U','D']:
        ret = zip(const, steps)
    else:
        ret = zip(steps, const)
    
    return list(ret)[1:]

def returnPoints(directions):
    points = []
    currentX = 0
    currentY = 0
    for direction in directions:
        dist = int(direction[1:]) + 1
        o = direction[0]
        if o in ['U', 'D']:
            steps = returnSteps(currentX, currentY, dist, o)
        else:
            steps = returnSteps(currentY, currentX, dist, o)
        
        for step in steps:
            points.append(step)
        
        currentX, currentY = points[-1]
        
    return points
            
start = time.time()

with open('./inputs/day3.txt', 'r') as f:
    data = f.readlines()

#data = ['R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83']
data = list(map(lambda d: d.replace('\n','').split(','), data))

points1 = returnPoints(data[0])
points2 = returnPoints(data[1])

intersections = set(points1).intersection(set(points2))

i = list(intersections)
i.sort(key = lambda x: sum(map(abs, x)))
print(i[0], sum(map(abs, i[0])))

distances = list(map(lambda p: (points1.index(p), points2.index(p)), i))
print(min(map(sum, distances))+2)
print(time.time()-start)