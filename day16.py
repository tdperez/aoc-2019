# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from collections import defaultdict
from math import ceil
import time

start = time.time()
with open('./inputs/day16.txt', 'r') as f:
    data = f.read()

#data = '03036732577212944063491565474664'
stream = np.array([int(char) for char in data], dtype=np.int16)

basePhase = [0,1,0,-1]
phases = []

## Part 1
for i in range(len(stream)): #range(len(stream)):
    phase = [[bp]*(i+1) for bp in basePhase]
    phase = [pp for p in phase for pp in p]
    
    phaseEntry = []
    while len(phaseEntry) < len(stream)+1:
        phaseEntry += phase
        
    phases.append(np.array(phaseEntry[1:len(stream)+1], dtype=np.int32))

transform = 0
t = stream.copy()
while transform < 100:
    m = np.matmul(t, np.transpose(phases))
    t = abs(m)%10
#    print(''.join(list(map(str, t))))
    transform += 1
    
print(''.join(list(map(str, t)))[:8])
print(time.time()-start)
## Part 2

stream = np.array(list(stream)*10000, dtype=np.int16)
i = int(''.join(map(str, stream[:7])))
t = stream[i:]

transform = 0
while transform < 100:
#    i = int(''.join(map(str, stream[:7])))
    s = sum(t)
    n = t.copy()
    n[0] = s%10
    for idx in range(1, len(t)):
        s -= t[idx-1]
        n[idx] = s%10
#        if idx < 8:
#            print(n[:8])
#    break
    t = n.copy()
    transform += 1
    
print(''.join(list(map(str, t)))[:8])
print(time.time()-start)