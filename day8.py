# -*- coding: utf-8 -*-
"""
Created on Sun Dec  8 12:48:23 2019

@author: t.perez
"""

import numpy as np

with open('./inputs/day8.txt', 'r') as f:
    data = f.read()

## Part 1
numlayers = len(data)//(6*25)
argmax = 0
layers = {}
for l in range(numlayers):
    img = np.ndarray((6,25))
    lidx = l*6*25
    subdata = data[lidx:lidx+(6*25)]
    for rast in range(6):
        line = subdata[rast*25:(rast+1)*25]
        img[rast] = [int(digit) for digit in line]
    
    zeros = (img==0).astype(int)
    ones = (img==1).astype(int)
    twos = (img==2).astype(int)
    layers[l] = {'image':img,
                 'zeros': zeros.sum(),
                 'chcksum': ones.sum()*twos.sum()}
    if layers[l].get('zeros') < layers[argmax].get('zeros'):
        argmax = l

print(layers[argmax].get('chcksum'))

## Part2
finalImage = np.zeros((6,25))
finalImage = finalImage + 2
for l in layers:
    nextLayer = layers[l].get('image').copy()
    nextLayer[finalImage!=2] = 0
    finalImage[finalImage==2] = 0
    finalImage = finalImage + nextLayer

for r in finalImage:
    print(''.join(map(str, map(int, r))).replace('0',' '))