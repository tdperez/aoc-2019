# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from itertools import permutations
from queue import Queue
from functools import reduce


def getPosition(mode, param, p, offset):
    if mode == 2:
        if param + offset >= len(p):
            p.resize(param + offset + 1, refcheck=False)
        ret = param + offset
    else:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = param
    
    return p, ret

def getValue(mode, param, p, offset):
    if mode == 0:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = p[param]
        
    elif mode == 2:
        if param + offset >= len(p):
            p.resize(param + offset + 1, refcheck=False)
        ret = p[param + offset]
        
    else:
        if param >= len(p):
            p.resize(param + 1, refcheck=False)
        ret = param
    
    return p, ret

def mult(idx, p, modes, **kw):
    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    p[v3] = v1 * v2
    return p, 4, None, None

def add(idx, p, modes, **kw):
    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    p[v3] = v1 + v2
    return p, 4, None, None

def stor(idx, p, modes, ipt = None, **kw):
    i = ipt.get()
#    print(idx, modes[0], i, kw['offset'])
    p, pos = getPosition(int(modes[0]), p[idx+1], p, kw['offset'])
#    print(pos)
    p[pos] = i
    return p, 2, None, None

def out(idx, p, modes, **kw):
    i = p[idx+1]
    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, v, None

def jit(idx, p, modes, **kw):
    i1, i2 = p[idx+1:idx+3]
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v != 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    
    return p, retJump, None, None

def jif(idx, p, modes, **kw):
    i1, i2 = p[idx+1:idx+3]
    p, v = getValue(int(modes[0]), i1, p, kw['offset'])
    if v == 0:
        p, j = getValue(int(modes[1]), i2, p, kw['offset'])
        retJump = j - idx
    else:
        retJump = 3
    return p, retJump, None, None

def lst(idx, p, modes, **kw):
    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 < v2:
        p[v3] = 1
    else:
        p[v3] = 0
    return p, 4, None, None

def eql(idx, p, modes, **kw):
    i1, i2, i3 = p[idx+1:idx+4]
    p, v1 = getValue(int(modes[0]), i1, p, kw['offset'])
    p, v2 = getValue(int(modes[1]), i2, p, kw['offset'])
    p, v3 = getPosition(int(modes[2]), i3, p, kw['offset'])
    if v1 == v2:
        p[v3] = 1
    else:
        p[v3] = 0
    return p, 4, None, None

def ost(idx, p, modes, **kw):
    i = p[idx+1]
    p, v = getValue(int(modes[0]), i, p, kw['offset'])
    return p, 2, None, v

PROGS = {'01': add,
         '02': mult,
         '03': stor,
         '04': out,
         '05': jit,
         '06': jif,
         '07': lst,
         '08': eql,
         '09': ost,
         '99': 'stop'}

def compute2(inputq=None, prog=[], opcodeIdx=0, outputs=None, offset=0):
    while opcodeIdx < len(prog):
        if prog[opcodeIdx] == 99:
            ret = ((inputq, prog.copy(), opcodeIdx, outputs, offset), True, False)
            break
        else:
            opcode = f'{prog[opcodeIdx]:05}'
            op = opcode[3:5]
            modes = opcode[2::-1]
            if op == '03' and inputq.qsize() == 0:
                ret = ((inputq, prog.copy(), opcodeIdx, outputs), False, True)
                break
            
            try:
                prog, increment, o, a = PROGS[op](opcodeIdx, prog, modes, ipt=inputq, offset=offset)
            except Exception as e:
                print(op, modes, opcodeIdx, len(prog))
                raise e
                
            if o is not None:
                outputs.put(o)
            
            if a is not None:
                offset += a
            opcodeIdx += increment

    return ret

def getNextIterator(i):
    if i == 4:
        return 0
    else:
        return i+1
    
with open('./inputs/day9.txt', 'r') as f:
    prog = list(map(lambda i: int(i), f.read().split(',')))

PROG = np.array(prog, dtype=np.int64) 
#PROG = np.array([1102,34915192,34915192,7,4,7,99,0], dtype=np.int64)

## Part 1
inpt = Queue()
otpt = Queue()

inpt.put(1)

state, complete, stalled = compute2(inputq=inpt,prog=PROG.copy(),opcodeIdx=0,outputs=otpt,offset=0)
outputs = []
while not otpt.empty():
    outputs.append(otpt.get())
    
print(outputs)

## Part 2
inpt.put(2)
state, complete, stalled = compute2(inputq=inpt,prog=PROG.copy(),opcodeIdx=0,outputs=otpt,offset=0)
outputs = []
while not otpt.empty():
    outputs.append(otpt.get())
    
print(outputs)