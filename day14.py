# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 09:43:25 2019

@author: t.perez
"""

import numpy as np
from collections import defaultdict
from math import ceil


def splitReactants(r):
    ret = []
    rs = r.split(', ')
    for r in rs:
        v, p = r.split(' ')
        ret.append((int(v),p))
    return ret


def getRequirements(p, a, RXNS):
    if a <= 0:
        return {}
    else:
        ret = []
        comps = RXNS[p]
        for p_a in comps.keys():
            if p_a < a:
                multiplier = np.ceil(a/p_a)
                for v, c in comps[p_a]:
                    ret.append((multiplier*v, c))
#                print(p, a, p_a, multiplier, ret)
            else:
                multiplier = 1
                ret = comps[p_a]
        
        return {multiplier*p_a: ret}
        
with open('./inputs/day14.txt', 'r') as f:
    reactions = f.read().split('\n')

#reactions = ['171 ORE => 8 CNZTR',
#'7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL',
#'114 ORE => 4 BHXH',
#'14 VRPVC => 6 BMBT',
#'6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL',
#'6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT',
#'15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW',
#'13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW',
#'5 BMBT => 4 WPTQ',
#'189 ORE => 9 KTJDG',
#'1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP',
#'12 VRPVC, 27 CNZTR => 2 XDBXC',
#'15 KTJDG, 12 BHXH => 5 XCVML',
#'3 BHXH, 2 VRPVC => 7 MZWV',
#'121 ORE => 7 VRPVC',
#'7 XCVML => 6 RJRHP',
#'5 BHXH, 4 VRPVC => 5 LTCX']

rxn = {}
for reaction in reactions:
    reactants, products = reaction.split(' => ')
    a, p = products.strip().split(' ')
    rxn[p] = {int(a): splitReactants(reactants)}
    
## Part 1
store = {}
requiredComponents = {'FUEL': 1}
stored = True
while not (len(requiredComponents.keys()) == 1 and 'ORE' in requiredComponents):
    needed = set(requiredComponents.keys())
    if 'ORE' in needed:
        needed.remove('ORE')
    rc = needed.pop()
    amtReq = requiredComponents.get(rc)
    if stored:
        store.setdefault(rc,0)
        amtStr = store[rc]
        reuse = min(amtReq, amtStr)
        if amtStr < amtReq:
            amtReq -= amtStr
            store[rc] = 0
        elif amtReq < amtStr:
            store[rc] -= amtReq
            amtReq = 0
        else:
            amtReq = 0
            store[rc] = 0

        

    reqRxn = getRequirements(rc, amtReq, rxn)
    
    for k in reqRxn.keys():
        if k > amtReq:
            store[rc] = k - amtReq + store.get(rc,0)
        for cmpA, cmp in reqRxn.get(k):   
            requiredComponents[cmp] = cmpA + requiredComponents.get(cmp, 0)
    requiredComponents.pop(rc)
#    print(requiredComponents, list([store.get(c) for c in requiredComponents.keys()]))
# 359282 - 333831
    
print(f'Needed ore: {requiredComponents["ORE"]}')

## Part 2
ore = 1000000000000
store = {}
fuel = 0
while ore > 0:
    f = max(int(np.log10(ore)) - 6, 0)
    requiredComponents = {'FUEL': 10**f}
    stored = True
    while not (len(requiredComponents.keys()) == 1 and 'ORE' in requiredComponents):
        needed = set(requiredComponents.keys())
        if 'ORE' in needed:
            needed.remove('ORE')
        rc = needed.pop()
        amtReq = requiredComponents.get(rc)
        if stored:
            store.setdefault(rc,0)
            amtStr = store[rc]
            reuse = min(amtReq, amtStr)
            if amtStr < amtReq:
                amtReq -= amtStr
                store[rc] = 0
            elif amtReq < amtStr:
                store[rc] -= amtReq
                amtReq = 0
            else:
                amtReq = 0
                store[rc] = 0
    
            
    
        reqRxn = getRequirements(rc, amtReq, rxn)
        
        for k in reqRxn.keys():
            if k > amtReq:
                store[rc] = k - amtReq + store.get(rc,0)
            for cmpA, cmp in reqRxn.get(k):   
                requiredComponents[cmp] = cmpA + requiredComponents.get(cmp, 0)
        requiredComponents.pop(rc)
    ore -= requiredComponents.get('ORE')
    fuel += 10**f
#    if fuel%10000 == 0 or ore < 1000000:
#        print(ore, fuel, f)
if ore < 0:
    print(ore, fuel-1)
else:
    print(ore, fuel)
        